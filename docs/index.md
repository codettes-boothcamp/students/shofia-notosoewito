# About me

![Screenshot](img/shofianoto.jpg)

HALLO EVERYONE!!

I'm Shofia Notosoewito from Lelydorp, Suriname. Thirthy years old and driven to learn more about technology, teaching and what life has too offer.   

I graduated from the Anton de Kom University, transision year. Than started to work in a company and they saw in me the potential in ICT, I think because I like physics and math. So I started to read and learn more about it and did some basic coures like A+ PC technician and Network+. 

In 2013 I partisipated in my first *hackathon* held by IT-Core, a non-profit ICT organisation. And again in 2015 and 2016 and won first place. In that year I also partisipated in *fishackathon* by WWF GUIANAS

In 2015 I decided to volunteer in IT-Core to network with others and develop my experiences and skills in different projects and events. In the next year we started *Robokidz* classes. Other events are *hackathons*, *fishackathon* and *hackomation*.

## Project

For my final project I want to make a *smart doorlock with NFC*. The lock that can open and lock with a NFC cards or tags. A smartlock that can be monitored and give remotely access to someone who does not have a card or tag.

![Screenshot](img/sketch1.jpg)