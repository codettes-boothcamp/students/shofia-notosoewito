## W3 & 4: Embedded Programming

In week 3 we had focus on *Embedded Programming* as we started with some basic electronics. There are two types of electrical signals, the alternating current(AC) and direct current (DC). You have circuits that can be open(I) or close(O).
and you have series and parallel connection. You also have resistors, capacitors, diodes, transistors, potentiometers, LEDs, switches, wire, connectors etc.

For Embedded Programming we uses the Arduino Uno Kit and the Arduino 1.8.9 software. In the software we are writing in C-language and the coding is called sketch.  

![arduino](../img/emp/arduino.png)

### Assigment

- Upload Blink
- Do an spaceship interface with 3 LEDs
- Procedural programming with running LEDs
- Serial communication
- LDR (light dependent resistor)
- PWM (pluse- width modulation)
- DHT (Temperature and humidity sensor)
- Motor control with DC motor
- Motor control with servo motor

### Blink

Install the [Arduino IDE](https://www.arduino.cc/en/Main/Software.com) and open LED blink sketch.  
Go to File>>Examples>>01.Basic>>Blink

![blink](../img/emp/blink.png)

Than connect the board on your pc; select which one your using.  
Go to Tools>>Board>>Arduino/Genuino Uno

![board](../img/emp/board.png)

Before uploading the code make sure you are connected to the right port.  
Go to Tools>>Port>>COM12

![port](../img/emp/port.png)

First verify then upload the code by clicking on the first verify followed by the upload toggle.

![verify](../img/emp/verify.png)

### Spaceship Interface

For the spaceship interface we used the breadboard, LEDs, 220-ohm and 10k-ohm resistor, switch, and connectors.  
First wire up the breadboard.

![spaceinter](../img/emp/spaceinter.png)

For the *spaceship interface* you have three parts of coding:  
1. The declaration part. In this line your telling a state.  
2. The setup part. Here you define pins.  
3. The Loop. The loop function runs over and over again forever  

```
int switchState=0; 		// this is the first line to declare something starting with "int" stands for integer (this is a data type)  
						// the switch is inially set as 0 (zero)  
void setup(){			// this is the second part of coding where you devine your pins whether it's INPUT or OUTPUT  
	pinMode(3,OUTPUT);		// setting pin 3 as an output  
	pinMode(4,OUTPUT);
	pinMode(5,OUTPUT);
	PinMode(2,INPUT);		//  setting pin 2 as an input  
	}
									
void loop(){						// in a loop first you assigment on the state (the declaration part) followed by a ''if-else'' statement.  
	switchState=digitalRead(2);		// = wordt  
	if(switchState==0){				// == comparison	
		digitalWrite(3,1);
		digitalWrite(4,0);			//if(){}  
		digitalWrite(5,0);
		}
	else{							//else{} statement  
		digitalWrite(3,0);
		digitalWrite(4,0);
		digitalWrite(5,1);
		delay(250);					//wait for 250 sec  
		digitalWrite(4,1);
		digitalWrite(5,0);
		delay(250);
	}	}
```

### Procedural Programming with running LEDs

The *procedural programming* you have 4 parts of coding:  
1. The Declaration.  
2. The Setup.  
3. The Subprocedures/subrotine.  
4. The Loop.  

In a procedural programming your making subrotine for often used of the procedures or instructions

```
int switchState = 0;		// the state

void setup() {				// the setup function runs once when you press reset or power the board			
  pinMode(2, INPUT);		// initialize digital pin 2 as an input  
  pinMode(3, OUTPUT);
  pinMode(4, OUTPUT);
  pinMode(5, OUTPUT);
}
void LedsOff(){				// subrotine 1
    digitalWrite(3,LOW);
    digitalWrite(4,0);
    digitalWrite(5,0);
}
void LedsRunning(){			// subrotine 2
    LedsOff();				// here we are using subrotine 1 LedsOff
    digitalWrite(3,1);
    delay(250);
    LedsOff();
    digitalWrite(4,1);
    delay(250);
    LedsOff();
    digitalWrite(5,1);
    delay(250);
}

void loop() {				// the loop function runs over and over again forever
  switchState = digitalRead(2);
 if(switchState==0){
    LedsOff();
 }else{
    LedsRunning();			// here we are using subrotine 2 ledsRunning

  }
}
```
### Serial Communication

For serial communication we are using the Arduino board and computer(Arduino IDE).  The Arduino Uno have 2 serial communication pin  (pin0 RX, and pin1 TX)
![pin](../img/emp/TxRx.jpg)

You can use the Arduino environment’s built-in serial monitor to communicate with an Arduino board.
Go to tools>> serial monitor
![monitor](../img/emp/sermonitor.png)

Built-in monitor.
![monitor](../img/emp/monitor.png)

```
void setup() {
	Serial.begin(9600);					// setup your serial
	Serial.println(“hello codettes”);
}

void loop() {
	Serial.println(millis());			// Send a message to your serial port/monitor
	delay(2000);
}

```

### LDR (Light dependent resistor)

LDR is a type of resistor that allows higher voltages to pass through it (low resistance) whenever there is a high intensity of light, and passes a low voltage (high resistance) whenever it is dark. It is sensing the intensity of light in its enviroment.
```
int lightInt= 0;
//int analogPin= A0;
int minLight=800;
int maxLight=1023;
int lightPc=0;                          // the setup routine runs once when you press reset
//int sensorValue= 0;

void setup() {                           //void (omdat je er niet meer te maken heb)
       Serial.println("Party");
       Serial.begin(9600);  
       pinMode(3,OUTPUT);
       pinMode(4,OUTPUT);
       pinMode(5,OUTPUT); 
}                                       // initialize serial communication at 9600 bits per second
                                        //beginnen een serial connectie met een 9600 bits per sec
void LedsOff(){
    digitalWrite(3,LOW);
    digitalWrite(4,0);
    digitalWrite(5,0);
}

void LedsRunning(){
    LedsOff();
    digitalWrite(3,1);
    delay(250);
    LedsOff();
    digitalWrite(4,1);
    delay(250);
    LedsOff();
    digitalWrite(5,1);
    delay(250);                                   // the loop routine runs over and over again forever:
}

void loop() {
    lightInt = analogRead(A0); 
    lightPc = (lightInt-minLight)*100L/(maxLight-minLight); // read the input on analog pin 0
    Serial.println(lightPc); 
    LedsOff();
      if(lightPc >=30){digitalWrite(3,1);};
      if(lightPc >=60){digitalWrite(4,1);};
      if(lightPc >=90){digitalWrite(5,1);};
      if(lightPc >=95){LedsRunning();};                       
      delay(1000);
}
```

### PWM (Pulse-width modulation)


### DHT (Temperature and Humidity sensor)


### Motor control (DC motor)
**NPN transistor** 
![npntransistor](../img/emp/npn.png) 
**H-bridge** 

![hbridge](../img/emp/hbridge.png)
```
const int pwm = 3 ; //initializing pin 2 as pwm
const int in_1 = 8 ;
const int in_2 = 9 ;
//For providing logic to L298 IC to choose the direction of the DC motor

void setup() {
   pinMode(pwm,OUTPUT) ; //we have to set PWM pin as output
   pinMode(in_1,OUTPUT) ; //Logic pins are also set as output
   pinMode(in_2,OUTPUT) ;
}

void loop() {
   //For Clock wise motion , in_1 = High , in_2 = Low
   digitalWrite(in_1,HIGH) ;
   digitalWrite(in_2,LOW) ;
   analogWrite(pwm,250) ;
   /* setting pwm of the motor to 255 we can change the speed of rotation
   by changing pwm input but we are only using arduino so we are using highest
   value to driver the motor */
   //Clockwise for 3 secs
  delay(3000) ;
   //For brake
  digitalWrite(in_1,HIGH) ;
  digitalWrite(in_2,HIGH) ;
  delay(1000) ;
   //For Anti Clock-wise motion - IN_1 = LOW , IN_2 = HIGH
  digitalWrite(in_1,LOW) ;
  digitalWrite(in_2,HIGH) ;
  delay(3000) ;
   //For brake
  digitalWrite(in_1,HIGH) ;
  digitalWrite(in_2,HIGH) ;
  delay(1000) ;
}
``` 
**speed control (potentiometer)**

For speed control we are using the potentiometer
![speedcontrol pot meter](../img/emp/speedPot.png)  
```
// Declare ur variables
const int pwm = 3;
const int in_1 = 8;
const int in_2 = 9;


void setup(){
	pinMode(in_1, OUTPUT);	
  	pinMode(in_2, OUTPUT);
  	pinMode(3, OUTPUT);
  	Serial.begin(9600);
}
void loop(){
  	int duty = (analogRead(A0)-512)/2;
  	Serial.println(duty);
  	analogWrite(pwm,abs(duty));
  	if(duty>0){
        // turn CW
      digitalWrite(in_1,LOW);
      digitalWrite(in_2,HIGH); 
    }
  	if(duty<0){
        // turn CCW
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,LOW); 
    }
  	if(duty==0){
        // BRAKE
      digitalWrite(in_1,HIGH);
      digitalWrite(in_2,HIGH); 
    }  
}

```

**job bottom direction with mapping function** 

### Motor control (Servo)

![servo control](../img/emp/servoCon.png) 
```
#include <Servo.h>

Servo myServo;

int const potPin= A0;
int potVal;
int angle;

void setup (){
  myServo.attach(5);
  Serial.begin(9600);
}

void loop(){
  potVal= analogRead(potPin);
  Serial.print("potVal:");
  Serial.print(potVal);
  angle= map(potVal,0,1023,0,179);
  Serial.print(",angle:");
  Serial.println(angle);
  //analogWrite(5, map(potVal,0,1023,0,127));
  myServo.write(angle);
  delay(15);
}
```