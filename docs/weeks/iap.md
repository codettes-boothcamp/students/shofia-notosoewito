## W6 & 7: Interface and Application Programming

For the Interface and Application Programming we are working with the Raspberry Pi. I've chosen to work with a Raspberry Pi 3; **Rpi3**.  

### Assigment

- Install and configure the Rpi3.  
- Install Nodejs, folder structure and run the Express App.  
- Install Python, folder structure and create an simple website using Flask.  
- Interface the Arduino Uno with the Rpi3.  

### Raspberry pi 3

The Raspberry Pi is a microcomputer that people use to learn computing. The Pi also allows you to control electronic components for physical computing and explore the Internet of Things (IoT).
The Pi runs on a **Linux-based** operating system.
To start with the Pi first download and install [Disk32Imager](https://sourceforge.net/projects/win32diskimager/files/latest/download), [PuTTY](https://www.puttygen.com/download-putty), [WinSCP](https://winscp.net/eng/download.php) and [Angry IP scanner](https://angryip.org/download/#windows).

#### Raspberry Pi 3 Model B specification

**CPU**: Quad Core 1.2GHz Broadcom BCM2837 64bit CPU, 1GB RAM.  
**Network**: BCM43438 wireless LAN and Bluetooth Low Energy (BLE) on board, 100 Base Ethernet.  
**Ports**: 40-pin extended GPIO, 4 USB 2 ports, 4 Pole stereo output and composite video port, HDMI, CSI camera port, DSI display port, 
Micro SD port for loading your operating system and storing data.  
**Power**: Micro USB power source up to 2.5A.  

#### Setting up the Pi 3

For the Rpi3 you will need a good **power supply** at least 2.5 amps. Secondly a **SD-card** to store all its files and the Raspbian operating system, minimum of 32 GB and an **Ethernet cable** (crossover cable for peer to peer connection with your computer or an straight cable to connect to a network).

#### Install Raspbian operating system on SD-card

Download the operating system [Raspberry Wheezy](https://www.raspberrypi.org/downloads/) image file for the Rpi3. 
Than insert your SD-card in the SD-card port on your computer. Open Win32diskmanager, choose the Wheezy image file and write it on the SD-card drive.

![Win Disk Imager](../img/iap/winDiskim.png)

Before disconnecting the SD-card. Open the driver and edit with Notepad++ in **cmdline.txt** an ip-address.

![cmdline](../img/iap/cmdline.jpg)

Also add a text-document called **ssh** with no extention (type file).
 
![ssh](../img/iap/ssh.jpg)

The ip-address is for the Rpi3 to connect to the network for communication; localhost or network interface identification and location addressing. The **SSH** is a network protocol that **PuTTY** uses to remotely access the Rpi3; by adding the file u manually enable **SSH** on the first boot of your Rpi3.

#### Setup for localhost communication

First configure pc ethernet adapter to match the Rpi3 subnet. Open Network and Sharing Center> Change adapter setting> right click on Ethernet and select Properties.

![Network and Sharing Center](../img/iap/nsc.jpg) 

![Change adapter](../img/iap/adap.jpg)

In **Ethernet Properties** select **Internet Protocol Version 4** than click on properties. In the **TCP/IPv4 properties** u manually insert IP address and Subnet mask and click **OK**. Add a different address.

![Ethernet Properties](../img/iap/ep.jpg)   ![Internet Protocol Properties](../img/iap/ipp.jpg)

You than insert you crossover cable in your ethernet port of your computer and the other connecting to your Rpi3.

#### Setup network communication

For the network communication you will need a straight ethernet cable and connect that on a local area network (LAN) whether it is your router or a switch.

#### Connect your Rpi3

To connect your Rpi3 you have to do this in the right order, so that all your components are safe. First insert the SD-card into the microSD card slot, second you insert the Ethernet cable (crossover for local use or straight for network use). 
At last you connect the USB power into the socket of the power port. The red **LED** indicates the Pi is connected to power and as it starts up (booting) and the other light, (green) **LED**, will blink.

![booting](../img/iap/booting.jpg)

After connecting your Rpi3 go to your pc and ping ip-address of the Rpi3 to see if its available. Go to **command promt** and than type **ping 192.168.1.163** (ip-address that you've given your Pi).
You will get a reply of the ip-address.

![Command Promt](../img/iap/cp.jpg)

Open **PuTTY** and enter ip-address; give it a name; Save; and click on Open.

![PuTTY](../img/iap/putty.jpg)

Enter username and password. By default it will be username: **pi** and password: **raspberry** (nothing will occur). To configure your Rpi need to run the Pi setup wizard. Command: **sudo raspi-config**. (sudo means super user)
In the **Software Configuration Tool** (raspi-config) you change User Password and enable all **Interfacing Options**.

![user and password](../img/iap/.jpg)

![raspi-config](../img/iap/raspcon.jpg)

![user and password](../img/iap/up.jpg)

![Interfacing](../img/iap/interfacing.jpg)

### Install Node.js

Node.js is a free open source server environment that runs on various platforms (Windows, Linux, Mac OS, etc) and it uses JavaScript on the server. 
JavaScript is the programming language for the Web, it can update and change both HTML and CSS and it can calculate, manipulate and validate data.
Node.js is already available in the Raspbian Wheezy file.  
To check the version of node.js command in PuTTY: **node --version** and **npm --version**.  
If the file not include command: **curl -sL https://deb.nodesource.com/setup_8.x | sudo -E bash -**. (to download node.js)
To install Node.js command: **sudo apt-get install nodejs npm**  

![Install nodejs](../img/iap/nodejs.jpg)  

![Download nodejs](../img/iap/dlnodejs.jpg)

***Basic Linux command***

To communicate with the Rpi we are using Basic Linux command.

- cd; navigate through directories
- cd; go back to root directory
- cd.. ; go back to previous directory
- ls; list
- dir; directories
- mkdir; make a directory
- touch; make text file
- rm; remove files
- rmdir; remove directories

***Folder structure***

Create a folder structure for the Node.js 
 
- **nodejs**
> - **projects**
>> - **project1:**
>>> - index.js
>>> - **public**
>>>> - index.html
>>>> - **css**
>>>>> - style.css
>>> - package.json (created by default)

#### Bootstrapping Nodejs

All npm **(Node Package Manager)** packages contain a file, usually in the project root, called **package.json**; 
this file holds various metadata relevant to the project. 
This file is used to give information to npm that allows it to identify the project as well as handle the project's dependencies. 
It can also contain other metadata such as a project description, the version of the project in a particular distribution, 
license information, even configuration data; all of which can be vital to both npm and to the end users of the package. 
The **package.json** file is normally located at the root directory of a Node.js project.

With **PuTTY** you have created folder name **project**1. 
Navigate and standing in the directory **project**1 you install cmd: **sudo apt-get install nodejs npm** (so we can use the command-line utility for interacting). 
Cmd: **cd nodjes/projects/project1**.

Cmd: **npm init -y** (by doing this you initializ the package manager for Node.js packages and creating by default a package.json file). 
Create in directory **project**1 an **index.js** file. cmd: **touch index.js**. 

To run the application you need to create an npm script. For that you'll use **nodemon** to monitor your project source code and automatically restart your Node.js server whenever it changes.
Cmd: **npm i D nodemon** (install nodemon development dependency).  

![nodemon](../img/iap/nodemon.jpg)

#### Integrated and Serve up the Express app

To run the Express framework in your application you first need install the **Express App**. cmd: **npm i express**.
Than open the **index.js** file  with **notepad++** using **WinSCP** and add the following template that definces the code structure of the Express Application.

![expess](../img/iap/npmexpress.jpg)

![WinSCP](../img/iap/winscpl.jpg)

![WinSCP](../img/iap/winscp.jpg)

Add the following code of the Express Application:
```
/**
 * Required External Modules
 */ 

const express = require("express");
const path = require("path");

/**
 * App Variables
 */

const app = express();
const port = process.env.PORT || "9000";

/**
 *  App Configuration
 */


app.use(express.static(path.join(__dirname, "public")));

/**
 * Routes Definitions
 */

/**
 * Server Activation
 */

app.listen(port, () => {
  console.log(`Listening to requests on http://localhost:${port}`);
});

```
After adding the code you also need to open **package.json** for the following code:
```
{
  "name": "project1",
  "version": "1.0.0",
  "description": "",
  "main": "index.js",
  "scripts": {
    "dev": "nodemon ./index.js"  //using the nodemon development dependency
  },
  "keywords": [],
  "author": "",
  "license": "ISC",
  "dependencies": {
    "D": "^1.0.0",
    "express": "^4.17.1",
    "nodemon": "^2.0.2"
  }
}
```
![Package json](../img/iap/nodepj1.jpg)

Add a basis HTML in your **index.html** file:
```
<html>
<body>
<h1>My first website</h1>
</body>
</html>
```
To serve up the Express app you run your **nodejs** server. cmd: **npm run dev**

![npm run dev](../img/iap/npmrundev.jpg)

Open your web browser and type in your ip-address with the port number `http://192.168.8.163:9000/` too see app in action.

![index.html](../img/iap/html1.jpg)

### Install Python

Just like **Nodejs** is **Python** a free open source server environment that works on different platforms (Windows, Mac, Linux, Raspberry Pi, etc).
It can be used for web and software development and system scripting.
To install **Python** you do cmd: **pip install**.
Create an directory **python**; cmd: **mkdir python**. In directory **python** do the following folder structure.  

***Folder structure***

- **python**
> - **projects**
>> - **project1:**
>>> - index.py
>>> - **static**
>>>> - **img**
>>> - **templates**
>>>> - index.html (cmd: touch index.html)
>>> - **css**
>>>> - style.css

#### Setup Python Web-server with flask

To install the Flask package for Python module we are using sudo pip; cmd **sudo pip install flask**. 
Open file **index.py** with **notepad++** using WinSCP and add the following code:

```
from flask import Flask, render_template

app = Flask(__name__)

@app.route('/') //this determines the entry point; the / means the root of the website, so http://192.168.1.163:5000/
def index():	//this is the name you give to the route; this one is called index, because it’s the index (or home page) of the website
    return render_template('index.html')	//this is the content of the web page, which is returned when the user goes to this URL
if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0'
```
#### Run HTML Web-page

Open the **index.html** with notepad++ and add a simple HTML document:

```
<!DOCTYPE html>
<html>
<head>
<title>Page Title</title>
</head>
<body>

<h1>My First Heading</h1>
<p>My first paragraph.</p>

</body>
</html>
```
Save the **index.html** file and then start the **Phyton** server.
Run the command **python index.py**

![Run python](../img/iap/runpy.jpg)

Load the page in your web browser to see your new **HTML** template displayed.

![load html python](../img/iap/loadhtmlpy.jpg)

### Interface the Arduino Uno with the Raspberry pi 3

For this interface we are using the Arduino Uno and the Rpi over **serial communication**.  

#### Setup Arduino Uno

Built a basic circuit with one LED and resistor with the Arduino. Than upload the **PhysicalPixel** example code to the Arduino.

![PhysicalPixel](../img/iap/physicalpix.jpg)

***Code explanation***

First; declaration/ statment 
```
const int ledPin = 13; // the pin that the LED is attached to
int incomingByte;      // a variable to read incoming serial data into
```
Second; void setup
```
void setup() {
	Serial.begin(9600);  		// initialize serial communication:
	pinMode(ledPin, OUTPUT);	// initialize the LED pin as an output:
}
```
Third; void loop
```
void loop() {
  if (Serial.available() > 0) {			// see if there's incoming serial data:   
    incomingByte = Serial.read();		// read the oldest byte in the serial buffer: 
	if (incomingByte == 'H') {		 	// if it's a capital H (ASCII 72),
      digitalWrite(ledPin, HIGH);		// turn on the LED:
    }
    if (incomingByte == 'L') {			// if it's an L (ASCII 76) 
      digitalWrite(ledPin, LOW);		// turn off the LED:
    }
  }
}
```

#### Setup Rpi3 using Python

Create a python file in **project2** cmd: **mkdir project2**. cmd: **cd project2** ;(give it any name with extension .py) cmd: **touch arduino.py**.  
Open **arduino.py** with Notepad++ and add the following code:

```
import serial		# Define the serial port and baud rate.
import time			# Ensure the 'COM#' corresponds to what was seen in the Windows Device Manager

ser = serial.Serial('/dev/ttyACM0', 9600)   # ttyACM0 serial port for pi 2 #/dev/ttyAMA0 pi 3
def led_on_off():
    user_input = input("\n Type on / off / quit : ")
    if user_input =="on":
        print("LED is on...")
        time.sleep(0.1) 
        ser.write(b'H') 
        led_on_off()
    elif user_input =="off":
        print("LED is off...")
        time.sleep(0.1)
        ser.write(b'L')
        led_on_off()
    elif user_input =="quit" or user_input == "q":
        print("Program Exiting")
        time.sleep(0.1)
        ser.write(b'L')
        ser.close()
    else:
        print("Invalid input. Type on / off / quit.")
        led_on_off()

time.sleep(2) # wait for the serial connection to initialize

led_on_off()
```

Than cmd: **sudo pip install pyserial**.  
Do a cmd: **lsusb** to seen on witch poort the Rpi3 is connected with the Arduino.  
/dev/ttyAMA0  (raspberry pi 2)  
/dev/ttyACM0 (raspberry pi 3)

#### Connecting Arduino with Rpi

![Setup Arduino and Rpi](../img/iap/setupArRpi.jpg)

Open PuTTY; navigate to the file position (project2) and run cmd: **python arduino.py**.

![Run serial com](../img/iap/runserial.jpg)

cmd: **“on”** 	turns led on  
cmd: **“off”**  turns led off   
cmd: **"q"**	program exiting  

### W8 & 9: HTML5, CSS3, JavaScript

In this week we are learning the bacis of "building your own website" with **HTML5**, **CSS3**, and **JavaScript**. 
Check out [W3school](https://www.w3schools.com/) "THE WORLD'S LARGEST WEB DEVELOPER SITE" for learning web technologies online. 

We are building a dashboard with the basic layout and adding some interactive tools too capture data. 
Interconnecting Websockets with MQTT over socket.io.

Work through this [HTML5 and CSS3 guide](https://drive.google.com/file/d/0BwwL5RPpTcRNRWNTVWZPRFhGSGJ6Q2xrbkE1VE5KUDBib2JN/view?usp=sharing)

### Assigment

- Learn to type code!
- Learn HTML5/CSS3
- Build your own basic website
- Styling your page
- Using Libraries, Charts & Button
- JavaScript Widgets & Interactions
- WebSocket & MQTT
- Nodejs socket.io
- MQTT over socket
- TinkerCad LCD
- Simulate DC charger tinkerCad

### Building the dashboard

Before you build your website you need some pre-work to have in place:

- Determine Message/Goal/Target audience (why the website)
- Make a site structure / [site map](https://www.quicksprout.com/creating-website-sitemap/) of pages and sections of website 
- Setup website folder structure
- Collect content
- Select the look & feel
- Build Mockups
- Target devices & browsers

Host your website on your **Rpi Nodejs** infrastructure. Make a new project folder  
cmd: **mkdir project2**

> - project2
>> - public
>>> - index.html
>>> - img
>>> - css
>>>> - style.css
>>> - fonts
>>> - js
>>>> - lib
>> - index.js

#### HTML5

**HTML** or **HyperText Mark-up Language** is a language to produce visual output on a website or modern web-app. 
The HTML5 standard, the latest version, was released by the W3C Consortium in 2014. 
W3C is a group that standardizes the world wide web languages and protocols to enable streamlined experience and integration between all browsers, and back-ends of participating members.

Introducing NEW Semantic TAGS that actually have more meaning to the standard `<DIV>` tags for layout.
```
Tag              Description
<article>    //  Defines an article in a document
<aside>      //  Defines content aside from the page content
<bdi>        //  Isolates a part of text that might be formatted in a different direction from other text outside it
<details>    //  Defines additional details that the user can view or hide
<dialog>     //  Defines a dialog box or window
<figcaption> //  Defines a caption for a <figure> element
<figure>     //  Defines self-contained content
<footer>     //  Defines a footer for a document or section
<header>     //  Defines a header for a document or section
<main>       //  Defines the main content of a document
<mark>       //  Defines marked/highlighted text
<meter>      //  Defines a scalar measurement within a known range (a gauge)
<nav>        //  Defines navigation links
<progress>   //  Represents the progress of a task
<rp>         //  Defines what to show in browsers that do not support ruby annotations
<rt>         //  Defines an explanation/pronunciation of characters (for East Asian typography)
<ruby>       //  Defines a ruby annotation (for East Asian typography)
<section>    //  Defines a section in a document
<summary>    //  Defines a visible heading for a <details> element
<time>       //  Defines a date/time
<wbr>        //  Defines a possible line-break
```
This is the basics of a HTML5 is its fixed structure that will be standard for each page. 
To make code more readable apply tabbed nesting of hierarchical elements (like below).
```
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
			<title>Title MyPage (shows in browser window)</title>
	</head>
	<body>
		<!-- body content starts here -->
		Here is the visible Content of the page......
	</body>
</html>
```
Based on your Mockups the Layout is done by creating different sections using Semantics (sensible names for elements in your HTML) to tell where the layout different elements will be displayed. 
![mockups](../img/iap/mockups.jpg)

Inside the `<BODY>` tag create the layout definitions. They will NOT show anything of layout yet. As we need to style it.
Add in your **index.html** the Semantic layout code of **HTML**.
```
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8" />
    <title>Title</title>
    <link href="css/style.css" rel="stylesheet" />
    <meta name ="viewport" content ="width=device-width, initial-scale=1.0">
</head>

<body>
    <header class ="mainHeader">
    	<img src="img/logo.png">
	<nav>
	      <ul>
	            <li><a href="#" class="active">Home</a></li>
	            <li><a href="#">About</a></li>
	            <li><a href="#">Portfolio</a></li>
	            <li><a href="#">Gallery</a></li>
	            <li><a href="#">Contact</a></li>
	  	</ul>
        </nav>
    </header>
    
    <div class="mainContent">
	    <div class="content">
    		<article class="articleContent">
	            <header>
	                <h2>First Article #1</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>
	            <content>
	            	<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
        		</content>
    		</article> 
    		
    		<article class="articleContent">
	            <header>
	                <h2>2nd Article #2</h2>
	            </header>
	            
	            <footer>
	            	<p class="post-info">Written by Super Woman</p>	            	
	            </footer>

	            <content>
	            	<p>This is the actual article content ... in this case a teaser for some other page ... read more</p>
        		</content>
    		</article> 
    		
	   </div>
	</div>
	
	<aside class="top-sidebar">
		<article>
			<h2>Top sidebar</h2>
			<p>Lorum ipsum dolorum on top</p>
		</article>
	</aside>
	
	<aside class="middle-sidebar">
		<article>
			<h2>Middle sidebar</h2>
			<p>Lorum ipsum doloru in the middle</p>
		</article>
	</aside>
	
	<aside class="bottom-sidebar">
		<article>
			<h2>Bottom sidebar</h2>
			<p>Lorum ipsum dolorum at the bottom</p>
		</article>
	</aside>
	
	<footer class="mainFooter">
		<p>Copyright &copy; <a href="#" title = " MyDesign">mywebsite.com</a></p>
	</footer>
		
</body>

</html>
```
Fix your code with [CodeBeautifyer](https://beautifier.io//).

#### CSS3(Styling)

**CSS3** or **Cascading Style Sheets** enables the separation of presentation and content, including layout, colors, and fonts. 
To improve content accessibility, more flexibility and control of presentation characteristics, web pages share formatting by specifying the relevant CSS in a separate **.css file**. 

Add the following code in your **style.css** file in the **css** folder.
```
/* body default styling */

body {
    /* border: 5px solid red; */
    background-image: url(img/bg.png);
    background-color: #000305;
    font-size: 87.5%;
    /* base font 14px */
    font-family: Arial, 'Lucinda Sans Unicode';
    line-height: 1.5;
    text-align: left;
    margin: 0 auto;
    width: 70%;
    clear: both;
}

/* style the link tags */
a {
    text-decoration: none;
}

a:link a:visited {
    color: #8B008B;
}

a:hover,
a:active {
    background-color: #8B008B;
    color: #FFF;
}

/* define mainHeader image and navigation */
.mainHeader img {
    width: 30%;
    height: auto;
    margin: 2% 0;
}

.mainHeader nav {
    background-color: #666;
    height: 40px;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainHeader nav ul {
    list-style: none;
    margin: 0 auto;
}

.mainHeader nav ul li {
    float: left;
    display: inline;
}

.mainHeader nav a:link,
mainHeader nav a:visited {
    color: #FFF;
    display: inline-block;
    padding: 10px 25px;
    height: 20px;
}

.mainHeader nav a:hover,
.mainHeader nav a:active,
.mainHeader nav .active a:link,
.mainHeader nav a:active a:visited {
    background-color: #8B008B;
    /* Color purple */
    text-shadow: none;
}

.mainHeader nav ul li a {
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}


/* style the contect sections */

.mainContent {
    line-height: 20px;
    overflow: none;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.content {
    width: 70%;
    float: left;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.articleContent {
    background-color: #FFF;
    padding: 3% 5%;
    margin-top: 2%;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.post-info {
    font-style: italic;
    color: #999;
    font-size: 85%;
}

.top-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.middle-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.bottom-sidebar {
    width: 21%;
    margin: 2% 0 2% 3%;
    padding: 3% 5%;
    float: left;
    background-color: #FFF;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter {
    width: 100%;
    height: 40px;
    float: left;
    border-color: #666;
    margin: 2% 0;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}

.mainFooter p {
    width: 92%;
    margin: 10px auto;
    color: #FFF;
}
```
**Link your style-sheet** in your **index.html** file in the inside `<head>` section with the following tag:
`<link href="css/style.css" rel="stylesheet" />`

**To build in page responsiveness**, meaning the page will be displayed different for each device/screen dimension add 
this line `<meta name = "viewport" content = "width=device-width, initial-scale=1.0">` to the `<head>` section.
Responsiveness allows the webpage to be viewed differently on devices like mobiles and Tablets. 
So we can alight layout sections different for devices based on the observed screen dimensions.  
 
This line tells the browser that the width of the screen should be considered the "Full Width" of the page. 
Meaning no matter the width of the device you are on, whether on desktop or mobile. 
The website will follow the width of the device the user is on. You can read more about the viewport meta tag at W3Schools.

After this the whole aspect of responsiveness is writting in the CSS file. 
Strategy is to create the full blown page CSS first and after that create a section tag like as shown below and add display format specific styling. 
Just overrule the CSS tags u want different than standard.

@media only screen and (min-width:150px) and (max-width:600){ }
```
/* Add responsiveness overrules values for different screen resolution */

@media only screen and (min-width:150px) and (max-width:600) {
    .body {
        width: 95%;
        font-size: 95%;
    }
    .mainHeader img {
        width: 100%;
    }
    .mainHeader nav {
        height: 160px;
    }
    .mainHeader nav ul {
        padding-left: 0;
    }
    .mainHeader nav ul li {
        width: 100%;
        text-align: center;
    }
    .mainHeader nav a:link,
    mainHeader nav a:visited {
        padding: 10px 25px;
        height: 20px;
        display: block;
    }
    .content {
        width: 100%;
        float: left;
        margin-top: 2%;
    }
    .post-info {
        display: none;
    }
    .topContent,
    .bottomContent {
        background-color: #FFF;
        padding: 3% 5%;
        margin-top: 2%;
        margin-bottom: 4%;
        border-radius: 5px;
        -moz-border-radius: 5px;
        -webkit-border-radius: 5px;
    }
    .top-sidebar,
    .middle-sidebar,
    .bottom-sidebar {
        width: 94%;
        margin: 2% 0 2% 0;
        padding: 2% 3%;
    }
}
```
To run your HTML page cmd: **node index.js**
![project2](../img/iap/runhtml2.jpg)

![project2](../img/iap/html2dash.jpg)

**To place your logo** you add your picture in the **img** folder. And link your path as the following tag: 
`<img src="img/logo.png">`

#### Charts and Button using Libraries

**Add Chart**.

Add an basic **LineChart** from [Chart.js](https://www.chartjs.org/) to Article #2 of our website.

[Download chart.js](https://sourceforge.net/projects/chartjs.mirror/files/v2.9.2/Chart.js/download) library and add to your **/js/li** folder.  
Open **index.html** than add code to host chart inside your `<head>`:
```<script src="js/lib/Chart.js/Chart.js"></script>```
```<script src="js/utils.js"></script>```  
Create an **utils.js** file in **/js/li** folder and add the following code:
```
'use strict';

window.chartColors = {
	red: 'rgb(255, 99, 132)',
	orange: 'rgb(255, 159, 64)',
	yellow: 'rgb(255, 205, 86)',
	green: 'rgb(75, 192, 192)',
	blue: 'rgb(54, 162, 235)',
	purple: 'rgb(153, 102, 255)',
	grey: 'rgb(201, 203, 207)'
};

(function(global) {
	var MONTHS = [
		'January',
		'February',
		'March',
		'April',
		'May',
		'June',
		'July',
		'August',
		'September',
		'October',
		'November',
		'December'
	];

	var COLORS = [
		'#4dc9f6',
		'#f67019',
		'#f53794',
		'#537bc4',
		'#acc236',
		'#166a8f',
		'#00a950',
		'#58595b',
		'#8549ba'
	];

	var Samples = global.Samples || (global.Samples = {});
	var Color = global.Color;

	Samples.utils = {
		// Adapted from http://indiegamr.com/generate-repeatable-random-numbers-in-js/
		srand: function(seed) {
			this._seed = seed;
		},

		rand: function(min, max) {
			var seed = this._seed;
			min = min === undefined ? 0 : min;
			max = max === undefined ? 1 : max;
			this._seed = (seed * 9301 + 49297) % 233280;
			return min + (this._seed / 233280) * (max - min);
		},

		numbers: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 1;
			var from = cfg.from || [];
			var count = cfg.count || 8;
			var decimals = cfg.decimals || 8;
			var continuity = cfg.continuity || 1;
			var dfactor = Math.pow(10, decimals) || 0;
			var data = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = (from[i] || 0) + this.rand(min, max);
				if (this.rand() <= continuity) {
					data.push(Math.round(dfactor * value) / dfactor);
				} else {
					data.push(null);
				}
			}

			return data;
		},

		labels: function(config) {
			var cfg = config || {};
			var min = cfg.min || 0;
			var max = cfg.max || 100;
			var count = cfg.count || 8;
			var step = (max - min) / count;
			var decimals = cfg.decimals || 8;
			var dfactor = Math.pow(10, decimals) || 0;
			var prefix = cfg.prefix || '';
			var values = [];
			var i;

			for (i = min; i < max; i += step) {
				values.push(prefix + Math.round(dfactor * i) / dfactor);
			}

			return values;
		},

		months: function(config) {
			var cfg = config || {};
			var count = cfg.count || 12;
			var section = cfg.section;
			var values = [];
			var i, value;

			for (i = 0; i < count; ++i) {
				value = MONTHS[Math.ceil(i) % 12];
				values.push(value.substring(0, section));
			}

			return values;
		},

		color: function(index) {
			return COLORS[index % COLORS.length];
		},

		transparentize: function(color, opacity) {
			var alpha = opacity === undefined ? 0.5 : 1 - opacity;
			return Color(color).alpha(alpha).rgbString();
		}
	};

	// DEPRECATED
	window.randomScalingFactor = function() {
		return Math.round(Samples.utils.rand(-100, 100));
	};

	// INITIALIZATION

	Samples.utils.srand(Date.now());

	// Google Analytics
	/* eslint-disable */
	if (document.location.hostname.match(/^(www\.)?chartjs\.org$/)) {
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-28909194-3', 'auto');
		ga('send', 'pageview');
	}
	/* eslint-enable */

}(this));
```
Add to  Article 2 in your `<body>`:
```
<div style="width:100%; height:100%">
	<canvas id="canvas"></canvas>
	<button id="randomizeData">Randomize Data</button>
	<button id="addDataset">Add Dataset</button>
	<button id="removeDataset">Remove Dataset</button>
	<button id="addData">Add Data</button>
	<button id="removeData">Remove Data</button>
</div>
```

Add Javascript at bottom just above the closing tag of `</body>`:
```
<script>
	var MONTHS = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'];
	var config = {
		type: 'bar',
		data: {
			labels: ['January', 'February', 'March', 'April', 'May', 'June', 'July'],
			datasets: [{
				label: 'My First dataset',
				backgroundColor: window.chartColors.red,
				borderColor: window.chartColors.red,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				],
				fill: false,
			}, {
				label: 'My Second dataset',
				fill: false,
				backgroundColor: window.chartColors.blue,
				borderColor: window.chartColors.blue,
				data: [
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor(),
					randomScalingFactor()
				],
			}]
		},
		options: {
			responsive: true,
			title: {
				display: false,
				text: 'Chart.js Line Chart'
			},
			tooltips: {
				mode: 'index',
				intersect: false,
			},
			hover: {
				mode: 'nearest',
				intersect: true
			},
			scales: {
				xAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Month'
					}
				}],
				yAxes: [{
					display: true,
					scaleLabel: {
						display: true,
						labelString: 'Value'
					}
				}]
			}
		}
	};

	window.onload = function() {
		var ctx = document.getElementById('canvas').getContext('2d');
		window.myLine = new Chart(ctx, config);
	};

	document.getElementById('randomizeData').addEventListener('click', function() {
		config.data.datasets.forEach(function(dataset) {
			dataset.data = dataset.data.map(function() {
				return randomScalingFactor();
			});

		});

		window.myLine.update();
	});

	var colorNames = Object.keys(window.chartColors);
	document.getElementById('addDataset').addEventListener('click', function() {
		var colorName = colorNames[config.data.datasets.length % colorNames.length];
		var newColor = window.chartColors[colorName];
		var newDataset = {
			label: 'Dataset ' + config.data.datasets.length,
			backgroundColor: newColor,
			borderColor: newColor,
			data: [],
			fill: false
		};

		for (var index = 0; index < config.data.labels.length; ++index) {
			newDataset.data.push(randomScalingFactor());
		}

		config.data.datasets.push(newDataset);
		window.myLine.update();
	});

	document.getElementById('addData').addEventListener('click', function() {
		if (config.data.datasets.length > 0) {
			var month = MONTHS[config.data.labels.length % MONTHS.length];
			config.data.labels.push(month);

			config.data.datasets.forEach(function(dataset) {
				dataset.data.push(randomScalingFactor());
			});

			window.myLine.update();
		}
	});

	document.getElementById('removeDataset').addEventListener('click', function() {
		config.data.datasets.splice(0, 1);
		window.myLine.update();
	});

	document.getElementById('removeData').addEventListener('click', function() {
		config.data.labels.splice(-1, 1); // remove the label first

		config.data.datasets.forEach(function(dataset) {
			dataset.data.pop();
		});

		window.myLine.update();
	});
</script>
```

### JavaScript

#### Interactions (WebSocket)

Back-end communications
Setup server back-end (nodejs)
Connect over WebSocket & MQTT

#### Widgets

To create a widget requires four steps:
Design the widget layout. At the very least, you will need one layout file describing your widget layout. ...
Extend AppWidgetProvider. ...
Provide the AppWidgetProviderInfo metadata. ...
Add the widget to your application manifest.


#### Websockets Chat App

### MQTT

#### Interconnect NodeJs to MQTT


#### WebSockets with MQTT

#### Socket connect, subscribe, store, receive data

Storing received data points in client
Connecting your chart libraries
Connecting your chart to data
Chart Placeholder

#### Socket handle button

### Building the IoT Stack

For building the IoT stack example we worked with the Rpi3 GPIO pins (general-purpose input/output).
Click on the link for more [GPIO pins](https://www.raspberrypi.org/documentation/usage/gpio/) information.

For this example we are setting up the Nodejs Express App. Built the circuit on the Rpi using the GPIO pins connecting it to LEDs.
On the dashboard we are switching/ controlling the LEDs On or Off over WebSocket using the Socket.io library.

#### Setup Server in Nodejs

Setup your Rpi3 and login with PuTTy and WinSCP.
 
Create a new nodejs project; cmd: **mkdir quadrant1**. 
In the **quadrant1** directory install the express app; cmd: **npm install express**.   

![project quadrant](../img/iap/quadrant.jpg)

![project quadrant](../img/iap/quadrant8.jpg)

Make your folder structure:

- quadrant1  
> - node-modules  
> - public  
>> - js  
>>> - socket.js  
>>> - jquery.js  
>> - css  
>>> - app.css  
>> - index.html
> - app.js  
> - package-lock.json

***Setting up the back-end communication***

For the back-end communication we need to setup the **Socket.io** library.
Open with WinSCP your **app.js** file and add the following code:

![app.js](../img/iap/quadrant1.jpg)

Code for **app.js**
```
var express = require('express');
var app = express();
var server = require('http').createServer(app);
var io = require('socket.io').listen(server);
var path = require('path');
// Now setup the local hardware IO
var Gpio = require('pigpio').Gpio;
// start your server
var port=4000;
server.listen(port, (err) => {
  if (err) {
    return console.log('something bad happened', err)
  }

  console.log('server is listening on port 4000')
})

// routing your client app (stored in the /public folder)
app.use(express.static(path.join(__dirname, 'public')));


// Handling Socket messages  as soon as socket becomes active
io.sockets.on('connection', function (socket) {
	// Hookup button behaviour within socket to submit an update when pressed (TO DO)

	// when the client emits 'opencmd', this listens and executes
	socket.on('opencmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('opencmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('openvalve', socket.username, data);
			console.log("user: "+socket.username+" opens LED" + data);
			// add some error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(1);
		}, 1000)
	});

	// when the client emits 'closecmd', this listens and executes
	socket.on('closecmd', function (data) {
		// Add calls to IO here
		io.sockets.emit('closecmd', socket.username, data);
		setTimeout(function () {
			io.sockets.emit('closevalve', socket.username, data);
			console.log("user: "+socket.username+" close LED" + data);
			// add error handling here
			led = new Gpio(parseInt(data), {mode: Gpio.OUTPUT});
			led.digitalWrite(0);
		}, 1000)
		
		/*setTimeout(function () {
			led.digitalWrite(0);
			io.sockets.emit('closevalve', socket.username, data);
		}, 1000) */
	});

});
```
After adding the code; Open PuTTY and in directory **quadrant1** you install the **socket.io** and **pigpio**.

![project quadrant](../img/iap/quadrant5.jpg)

cmd: **npm install socket.io**  
cmd: **sudo apt-get install pigpio** (to install pigpio you need to be super user) 
 
#### Setup RaspberryPi

Built your Rpi circuit like in the picture below:

![project quadrant](../img/iap/quadrant6.jpg)

Check the mapping from the GPIO pins hardware and software.

![project quadrant](../img/iap/quadrant7.jpg)

The hardware GPIO pins 1 to 40 has in the software different function. For example pin 2 on the Rpi is 5V power, pin 3 is GPIO 2(SDA), pin 11 is GPIO 17 in the software.
So if you are connecting a LED on pin 11 you need to declear GPIO 17 in the software; the index.html.  

#### Setup WebSocket, HTML5 and CSS3

For the dashboard we need to setup the **index.html**. Open the **html** file and add the following code:

![project quadrant](../img/iap/quadrant2.jpg)

Code with explanation
```


```
To run the dashboard you serve up the Nodejs Express App.
cmd: **sudo node app.js** (you need to be super user to access GPIO)

![project quadrant](../img/iap/quadrant9.jpg)

![project quadrant](../img/iap/quadrant10.jpg)