## Final Project

As you can see I choose in the beginning for my project a *Smart Doorlock* that would work with an *NFC* (Near-Field-Communication). Through the course I switch to another project.

So for my Final Project I wanted to make something fun ans usefull for myself, my team. Because at home we love to race, I've decided to make a *Timming System*.

### Project Description

As for our final project we need to make a working prototype that can easly be develop further if requied. It needs to be investor readiness.

Therefore i've built a simple timing system that gives accurate time from start to finish. It is a light gate system that works with laser pointers. There are two station, one at the start line and second at finish line over a 400 meter distance (standard drag track). Each station consists of a laser pointer thats beams a light spot onto a phototransistor at the opposite side. By measuring the output of the laser, the system can detect when the laser beam has been interrupt. At the start line, that station will start the timer when the laser beam has interfere with an object, by this the wheel. At the second station when the laser beam interfere again the timer will stop. The resulting recorded time is displayed on the LCD screen and on a dashboard.

### Business Model Canvas
	
[Business Model Canvas](www.https://canvanizer.com/canvas/wjFD8gZsTUn0Q)

![business model canvas](../img/final/bmc.jpg)

### Design

#### Components

- Raspberry Pi3 1x
- ESP32 1x
- Adruino Uno 1x
- laser 2x
- Phototransistor 2x
- LCD 1x 
- Jumper cables 

#### Circuits



#### Progamming

#### Enclosures

#### Assemble and Test

### References

[Arduino Laser-based Timing System By Joe Palmer](https://www.instructables.com/id/Arduino-Laser-based-Timing-System/)

[Arduino Drag Race Light Tree by ea422](https://www.instructables.com/id/Arduino-Drag-Race-Light-Tree/)

[Wireless Laser-Gate Timing System for Track and Field © GPL3+](https://create.arduino.cc/projecthub/Pablerdo/wireless-laser-gate-timing-system-for-track-and-field-ba8cd9)
