## W1. Project Managment

### Assigment

- Work through a Git tutorial
- Create Personal Webpage on Gitlab (MkDocs)
- Sketch and Describe your Final Project Version 1.0

### Gitlab and Github Account

Register on [Gitlab](https://www.gitlab.com/users/sign_in),

![registergitlab](../img/pm/registergitlab.jpg)

than make an account in [Github](https://www.github.com/join). 

![accountgithub](../img/pm/accountgithub.jpg)

Download [Github Desktop](https://www.desktop.github.com/).

![gitdesktop](../img/pm/dt_inst.jpg)

![clone url](../img/pm/clone_url.jpg)

![clone local](../img/pm/clone_local.jpg)

### Personal Webpage on Gitlab

Install [*notepad++*](https://download.cnet.com/Notepad/3000-2352_4-10327521.html).
Than go to your local git file and open the index.md file with Notepad++.

![notepad++](../img/pm/np.jpg)
add to the file and save
![commit](../img/pm/commit.jpg)

![pull orgin](../img/pm/pull.jpg)

![push orgin](../img/pm/push.jpg)

![local host page](../img/pm/local_wppath.jpg)

![local host page](../img/pm/local_wplink.jpg)

![local host page](../img/pm/personal_wp.jpg)