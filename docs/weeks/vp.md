## W2. Video Production

This week we focus on video production and we are working with Adobe Premier Basics

### Assigment

- Setup green screen
- Editing in Adobe Primier Workspace
- Make you own video

### Green screen

The green screen setup:  
1. The green screen must be as flat as it can be with minimum wrinkels  
2. Object should be a meter or 2 infront of the screen  
3. Object should have enough space at the sides; 2 times arm lenght  
4. The camera must be on a fixed positon  
5. Use a 3.5mm mice  

### Adobe Premier Basics

Install [Adobe Premier](https://wetransfer.com/downloads/cbbecdbc1b34bfaef2fd571449dc279620191111014255/a516aa9786b6ff9127c2c7d45626d84920191111014255/96de94)
, open *Adobe Premier* and make a new project, click on file>> new>> project>> name new project>> OK.  

**Workspace**  
Now you are in your workspace. This is fully customizable in your comfort.  
In your workspace you have 4 panel, each with there own functions.  
1. You have the Project; in here you import files.  
2. Timeline; in here you drag a file from project and place it in you timeline where you do the editing.    
3. In Program you are playing what is in the timeline.  
4. In source you have options where you can edited different effects in each project individually.  

![Workspace](../img/vp/workspace.jpg)

**Importing files**

**Cutting and assembling the raw materials**

**Effect control**

**Adding titles to videos**

**Filmimg in sequence*

**Audio editing**

**File export**


