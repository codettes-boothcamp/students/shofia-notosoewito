## Computer Controlled Cutting

This week we did *Computer Controlled Cutting*. These are machine with a cutting knife movement that is controlled by a computer. Therefor there is a cutting table with a sharp object that will do the cutting while moving in any direction. Some examples are *CNC milling machine*, *Laser cutters* or *Fabric cutting machine* and *Vinyl cutting machine* etc.

### Assigment
- Make a CAD design using Parametric Design
- Export your CAD to a CAM preparing for CNC & Laser or Engraving production

### Digital Fabrication

Digital fabrication is a design and manufacturing workflow. First you start with a design, an CAD (Computer Aided Design). From a CAD you'll export it too a CAM (Computer Aided Machine) data file than sent the data file to a compatible Computer Controlled Cutting machine. For **digital fabrication** the most used machine are the **CNC milling**, **Laser cutter** and the **3D printer**.

**Workflow**

![WorkflowCAD-CAM](../img/cam/workflow.jpg)

**Most common forms of Digital Fabrication**

![cutting machine](../img/cam/cuttingmachine.jpg)

### Computer Aided Design (CAD)

As you can see, in the previous page we did CAD already in *AutoCAD Fusion360*, *Inkscape* and *TinkerCAD*. For a more precision 3D models for machine parts we will be using **OpenSCAD**.

**OpenSCAD**

**OpenSCAD** is a software for creating solid 3D CAD objects. It is free and available for Linux/UNIX, MS Windows and Mac OS X.
First download and install [OpenSCAD](https://www.openscad.org/index.html).

The OpenSCAD dashboard have 3 windows:

- Editor: The built-in text editor provides basic editing features like text search & replace and also supports syntax highlighting. There are predefined color schemes that can be selected in the Preferences dialog.
- Preview: Preview and rendering output. Using the Show Axes menu entry an indicator for the coordinate axes can be enabled.
- Console: Status information, warnings and errors are displayed in the console window.

![OpenSCAD Dashboard](../img/cam/openscaddash.jpg)

OpenSCAD provides two main modelling techniques: First there is constructive solid geometry (CSG) and second there is extrusion of 2D outlines. Autocad DXF files can be used as the data exchange format for such 2D outlines. In addition to 2D paths for extrusion it is also possible to read design parameters from DXF files. Besides DXF files OpenSCAD can read and create 3D models in the STL and OFF file formats.

Before you start with your 3D design you need too know some basics aspects of your CNC machine and the material you will be using.
CNC machine, operation and material:

- X,Y,Zmax coordinate
- Feed rate (F-mm/min) ; Fmax= 3000/ voor hout: Fmax= 1000
- Spindle RPM (S-rpm) ; Smax= 20K/ voor hout: Smax= 8-10K
- Pass-depth/step down (Z-mm) max; Zmax= 1/2d
- Controller: Grbl/ Lpt/ Serial/ USB
- Cut: inside cut/ outside cut/ pocket cut/ drilling  
- Tools/ Mills/ Toolchange
- Material: Size/ type

OpenSCAD also have ready-to-use **libraries/ Vitamins** of different kind of tools, shapes, and helpers to make OpenSCAD easier to use. [Download OpenSCAD libraries](https://github.com/nophead/NopSCADlib).

![OpenSCAD libraries](../img/cam/openscadlib.jpg)

Then go to ->> download folder --> right click and Extract-all the NopSCADlib-master.zip file

![OpenSCAD libraries](../img/cam/extractlib.jpg)

Select folder and click on **Extract**

![OpenSCAD libraries](../img/cam/extractlib1.jpg)

The location of the library is: This PC --> Documents --> OpenSCAD --> libraries

To use a few vitamins from the Library and align you need too include the library in the code in OpenSCAD.  For example:

```
include <NopSCADlib/vitamins/fuseholder.scad>
include <NopSCADlib/vitamins/geared_steppers.scad>

//fuse holder
translate([50, 0, 0])fuseholder(20);
//geared_stepper();
rotate([-90,0,0])geared_stepper(28BYJ_48);
```

**Assigment: speakerbox**

First you need to know your dimensions of your object and material what you will be using.

Open OpenSCAD go to --> File --> click New
In the Editor window you put the functions in for the rectangle or cube (dimensions), the pocket and the electronicsport. The dimensions are in millcentimeters (mm).

This will be your code for a 3D design
```
difference(){  
    cube([200,150,9]);     //200 mm is the x-axis, 150mm is the y-axis, 9mm is the z-axis
    translate([20,20,0])  //20mm offset from the x-axis and y-axis form where the cube should begin
        cube([18,100,9]);
    //pocket
     translate([100,75,0])  //the 100,75mm offset is the 0,0 point from the cylinder
        cylinder(5,12,12); //5mm is the depth, 12mm is radius1, 12mm is R2
    //electronicsport
     translate([200-40,150/2,0]) //0mm offset from the z-axis
        cylinder(9,30,30);      //for 0-9
 }
```
Click on F5 for Preview and F6 for Render. Now you have a 3D design that can not export to a 2D design. By adding function `projection` into the code you will have a 2D design.

```
projection(){}
```
Because you will not be able to see the pocket we change that code to:
```
//pocket
     translate([100,75,0])  //the 100,75mm offset is the 0,0 point from the cylinder
        cylinder(9,12,12); //9mm is the depth, 12mm is R1, 12mm is R2
```

![OpenSCAD speakerbox](../img/cam/speakerbox.jpg)

Then clik on Render. Go to --> File --> Export --> Export as SVG

![OpenSCAD speakerbox](../img/cam/exportSVG.jpg)

### Computer Aided Manifacture (CAM) 

#### CNC milling

For CNC (Computer Numerical Control) milling you will need G-codes which represents specific CNC fuction in alphanumeric format that is needed to controls a machine’s actions including speed, feed rate, coolants, etc. Every CNC machine used different G-codes. The G-codes drives the machine tool, such as drills, boring tools or lathes to fabricate or cut a material (metal, plastic, wood, ceramic, or composite). G-code is the most popular programming language used for programming CNC machinery.

**The G-code coordinate pipeline**:

- Unit conversion to metric
- Convert from relative to absolute and polar to
- Cartesian: g90g91XYZ()
- G52, G54, and G92 offsets
- G51 scaling
- G68 coordinate rotation

[Learn more about G-codes](https://roboticsandautomationnews.com/2018/01/26/how-to-become-a-g-code-master-with-a-complete-list-of-g-codes/15807/)

**Assigment: speakerbox**

Open [MakerCAM](uhttps://www.makercam.com/rl), an online software that generate G-codes. First you need to:

- Enable flash in browser
- Right click on the page and click --> Inspect
- Select HTML Element: Body --> Container --> Content 
- Change CSS Styles “height:100%” to “height:100vh” 

After that Click on File --> Open SVG file (speakerbox.svg). Try to drag your design to the center by selecting all elements; this will turn red.

![MakerCAM](../img/cam/makercam.jpg)

For each element you will change the setting to the right the parameters:
First select your basreflex element --> Click on CAM and select **profile operation** 

*Basreflex* parameters:

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 10
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

*Electrohole*:

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: inside
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 3
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

*Pocket*: For the pocket you select **pocket profile**

- tool diamter(mm): 6
- target depth(mm): -4 (remember; you don't want to cut through)
- safety height(mm): 10
- stock surface(mm): 0
- step over(%): 40
- step down(mm): 3
- roughing clearness(mm): 0
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

![Pocket](../img/cam/pocket.jpg)

*The main element*, select the outside profile and add to following parameters:

- tool diamter(mm): 6
- target depth(mm): -9
- inside/outside cut: Outside
- safety height(mm): 10
- stock surface(mm): 0
- step down(mm): 3
- feedrate(mm/minute): 1000
- plunge rate (mm/minute): 500
- direction: counter Clockwise

After editing all paramaters click on *CAM* --> *calculate all*. Then go to *Toolpath* and see if all profile parts names is there that you selected. If everything is there go back to *CAM* --> then click on *export G-code*. Select every Toolpath before exporting G-code. After selecting all toolpath click on *Export Selected Toolpath*, save the G-code and gave it a name. This G-code will be needed to cut the 2D design on a CNC machine.

![Toolpath gcode](../img/cam/toolpath.jpg)

**Prepare your CNC machine**

- Inspect the work table, check if it's level and secure your material.
- Check your machinery spindle and attach the required tooling, such as drill bits or end mills.
- Check your vacuum.

![CNC prep](../img/cam/cncstart.jpg)

- Load your Gcode file, we are using **UCCNC software for STEPCRAFT**.
- Line your 0,0 point with the CNC program, line your design with your material.
- Setup your z-axis point.

![CNC Gcode prep](../img/cam/cncscreen.jpg)

- Start the cutting.

![CNC cutting](../img/cam/cncprep.jpg)

Your result

![CNC end result](../img/cam/cncresult.jpg)

#### Laser cutting

The laser cutter is a machine that uses a laser to cut materials such as chip board, matte board, felt, wood, and acrylic up to 3/8 inch (1 cm) thickness. The laser cutter is able to modulate the speed of the laser head, as well as the intensity and resolution of the laser beam, and as such is able in both to cut and to score material, as well as approximate raster graphics. You have three process types of laser cutter.  

- **Laser cutting**

Laser cutting is a type of thermal separation process. The laser beam hits the surface of the material and heats it so strongly that it melts or completely vaporizes. Once the laser beam has completely penetrated the material at one point, the actual cutting process begins. The laser system follows the selected geometry and separates the material in the process. Depending on the application, the use of process gases can positively influence the result. The tooldiameter for laser cutting is 0.02 mm. Here you need a DKF/SVG file.  

- **The Laser Etching**

Laser etching, which is a subset of laser engraving, occurs when the heat from the beam causes the surface of the material to melt. The laser beam uses high heat to melt the surface of the material. The melted material expands and causes a raised mark. Unlike with engraving, the depth in etching is typically no more than 0.001 mm. Here you need a DNG/BMP file.  

- **The Laser Engraving**

Laser engraving is a process where the laser beam physically removes the surface of the material to expose a cavity that reveals an image at eye level. The laser creates high heat during the engraving process, which essentially causes the material to vaporize. It’s a quick process, as the material is vaporized with each pulse. This creates a cavity in the surface that is noticeable to the eye and touch. To form deeper marks with the laser engraver, repeat with several passes. Here you need SVG/DXF file.  

For laser cutting we will be using LaserWeb4/ CNCWeb, an opensource that is capable of running a Makerspace CAM operations. [Download and install LaserWeb4](https://github.com/LaserWeb/LaserWeb4-Binaries/).

Create your gcode steps:
- Set your machine parameters
- Import your DXF, SVG or BMP file (drag n drop)
- Select file for gcode generation
- View the code & save to file
 
 
#### 3D printer

3D printing, or additive manufacturing, is the construction of a three-dimensional object from a CAD model or a digital 3D model. They use a roll of thin plastic filament, melting the plastic and then depositing it precisely to cool and harden. They normally build 3D objects from bottom to top in a series of many very thin plastic horizontal layers. This process often happens over the course of several hours. The term "3D printing" can refer to a variety of processes in which material is deposited, joined or solidified under computer control to create a three-dimensional object. 


