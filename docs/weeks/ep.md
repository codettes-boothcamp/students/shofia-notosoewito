## W.11 & 12 Electronic Design en Production

For the electronic week we will start with a design. For the design we will be using **KiCAD**. This is an open source EDA software for Windows, OSX and Linux 
that can create PCB circuits for free with the most advanced features. 
To be able to print your CAD design we need to export the file too a **CAM(Computer- Aided Manifacturing)** data file.
For that we will be using the **FlatCAM** software. **FlatCAM** is free software that converts Gerber and Excellon (Drill) files to G-code, 
which you can then use to print or mill your design.


### Assigment

- Install KiCAD and create your PCB board
- Install FlatCAM and set parameters
- Set Prob Z physical and Heighmap for CNC milling machine
- Soldering
- Programming 

### KiCAD

First you need too download and install [KiCAD](https://kicad-pcb.org/download/). 
Than open KiCAD and create a new project and save it in a new folder; Electronic design. Go to File>> New>> Project. Name your file and Save.

![KiCAD project](../img/ep/kicad.jpg)

#### Schematic Capture

Before you create your design we will import the **mod-pretty library**. Download the [KiCAD library](https://gitlab.cba.mit.edu/pub/libraries/-/tree/master/kicad https://drive.google.com/file/d/1Biu_bRiWvbsMKatI72IZVVY9pUrQLORU/view?usp=sharing), 
unzipped the file and save the **kicad** folder in the previous folder; Electronic design. See folder path.

![KiCAD schematic](../img/ep/kicad2.jpg)

Too import the library; open the **.sch** (KiCAD Schematic) file. Than go to Preferences>> Manage Symbol Libraries... (click); Append library (to add a row)>> Browse Libraries>> select **fab.lib**>> OPEN.  

![KiCAD schematic](../img/ep/kicad3.jpg)

![KiCAD schematic](../img/ep/kicad1.jpg)

Now add components. On the right side-bar you click on **place symbol**; all the symbols of the libraries will be loading. 
Select all the components that is needed for your design in the **fab** library.

![KiCAD schematic](../img/ep/kicad5.jpg)

![KiCAD schematic](../img/ep/kicad4.jpg)

After selecting all your components you connect the pins to complete your circuit.

![KiCAD schematic](../img/ep/kicad6.jpg)

To connect the pins right look into pin configuration of the ATmega328p-Au.

![KiCAD schematic](../img/ep/kicad7.jpg)

Connect all the components with each other; **KiCAD schematic circuit**.

![KiCAD schematic](../img/ep/kicad8.jpg)

Next you **Annotate** your schematic symbols. This to number the components before moving on to the pcb layout. 
Click the Annotate Tool>>  set the params>> click **ANNOTATE** and kicad automatically numbers all the components.

![KiCAD schematic](../img/ep/kicad9.jpg)

#### PCB (Printed Circuit Board)

Unlike other PCB design tools, schematic components are not automatically linked to a footprint.  
In Kicad, we must assign PCB footprints to schematic symbols.

First we add schematic components to footprint libraries.
Go to Tools>> Assign footprint>> Preferences>> Manage Footprint Libraries...>> Append Library>> Browse Libraries;  
navigate too the **fab.mod** file>> OK>> OK; **fab** Library is added

![PCBnew](../img/ep/pcb.jpg)

You than double-click on the required footprint on the right panel, and with a component selected in the middle panel, 
the footprint and the component will combined/ linked.

![PCBnew](../img/ep/pcb1.jpg)

For every component you click on>> Apply, Save Schematic & Continue. If all component is assign with there footprint you click on>> OK.

Next you create a new folder: **Netlist** in your new **project** folder.
A Netlist file contains all the information that Pcbnew, the PCB layout program, 
will need in order to know which components are supposed to be included and what are the connections between those components information.
Than click on>> Generate Netlist.

![PCBnew](../img/ep/pcb2.jpg)

Click on>> Generate Netlist; Navigate too the **Netlist** folder and name your **.net** file and>> Save.

![PCBnew](../img/ep/pcb3.jpg)

Open your **.kicad_pcb** file and>> read netlist>> Read Current Netlist>> Save Report File.

![PCBnew](../img/ep/pcb4.jpg)

Kicad is able to detect new components in  a netlist file and only import the new components into Pcbnew. 
This allows you to make changes to your schematic and then import those changes into your PCB layout without losing any work.

![PCBnew](../img/ep/pcb5.jpg)
![PCBnew](../img/ep/pcb6.jpg)