## W5. Computer Aided Design (CAD)

This week we did CAD using the software *TinkerCad* and *Autodesk Fusion360*.

### Assigment

- Learn to model your final project enclosure a 3D design using TinkerCad
- Install Inkscape and Fusion360
- Install and configure 3D printer AnyCubic for Maxpro

## Autodesk TinkerCad

In the TinkerCad online software tool that we've used previous for the embedded programming we our now using the 3D Designs.

## Inkscape and Fusion360

Download and install [Inkscape](https://inkscape.org/release/inkscape-0.92.4/)


## AnyCubic 3D printer