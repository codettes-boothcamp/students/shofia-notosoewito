[LCD screen with shift register](https://www.instructables.com/id/How-to-Connect-I2C-Lcd-Display-to-Arduino-Uno/)

[Laser pointer for Alarm](https://www.instructables.com/id/Laser-Tripwire-Alarm/)

[Arduino-interrupt projects](https://www.best-microcontroller-projects.com/arduino-interrupt.html)

[interrupts](http://gammon.com.au/interrupts)

[esp32 gpio interrupts](https://lastminuteengineers.com/handling-esp32-gpio-interrupts-tutorial/)

[interrupts examples](https://circuitdigest.com/microcontroller-projects/arduino-interrupt-tutorial-with-examples)

[traffic lights](https://pmdway.com/blogs/arduino-projects/fun-with-arduino-controlled-traffic-lights)